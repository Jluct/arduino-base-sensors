#pragma once

#include <Arduino.h>
#include "Sensor.h"
#include "Converter.h"

String Converter::toJSON(Sensor **sensors, int size, int location = 0)
{
    char stringBuffer[8] = "";
    String out = String();
    out.concat("[");

    for (int i = 0; i < size; i++)
    {
        if (sensors[i]->location != location && location != 0)
        {
            continue;
        }

        if (out.length() > 1)
        {
            out.concat(",");
        }

        out.concat("{");
        out.concat("\"");
        out.concat("id");
        out.concat("\"");
        out.concat(":");
        out.concat(itoa(sensors[i]->id, stringBuffer, 10));
        out.concat(",");
        out.concat("\"");
        out.concat("data");
        out.concat("\"");
        out.concat(":");
        out.concat(itoa(sensors[i]->data, stringBuffer, 10));
        out.concat("}");
    }
    out.concat("]");

    return out;
};
