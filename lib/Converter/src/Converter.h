#pragma once

#include <Arduino.h>
#include "Sensor.h"

class Converter
{
  public:
    static String toJSON(Sensor **sensor, int size, int location = 0);
    static String toCSV(Sensor **sensor, int size, int location = 0);
    static String toXML(Sensor **sensor, int size, int location = 0);
};