#pragma once

#include <Arduino.h>

struct Device{
    unsigned int id;
    unsigned int location;
    int (*handler)(int data);
};