#include <Arduino.h>
#include "Request.h"

void Request::setRequestGET(char symbol)
{
    if (this->countGetRequest >= 30)
    {
        return;
    }

    if (symbol == '?')
    {
        this->isRequestRecord = true;
        return;
    }

    if (symbol == ' ' && this->isRequestRecord)
    {
        this->isRequestRecord = false;
        this->countGetRequest++;
        this->_GET[this->countGetRequest] = '\0';
        return;
    }

    if (this->isRequestRecord)
    {
        this->_GET[this->countGetRequest] = symbol;
        this->countGetRequest++;
    }
};

void Request::analyze()
{
    if (!this->countGetRequest)
    {
        return;
    }

    char *tmp;
    tmp = (char*)malloc(10);
    memset(tmp, 0, 10);
    
    int count = 0;

    bool startValue = false;

    for (int i = 0; i < this->countGetRequest; i++)
    {
        char c = this->_GET[i];

        if (c == '=' && !startValue)
        {
            this->params = (char **)realloc(this->params, 10 * (this->countParams + 1));
            this->data = (int *)realloc(this->data, sizeof(int) * (this->countParams + 1));
            count++;
            tmp[count] = '\0';
            char *param = (char*)malloc(count);
            strncpy(param, tmp, count);
            this->params[this->countParams] = param;
            this->data[this->countParams] = 0;

            this->countParams++;
            startValue = true;
            memset(tmp, 0, count);
            count = 0;
            continue;
        }

        if ((c == '&' || c == ' ' || c == '\0') && startValue)
        {
            this->data[this->countParams - 1] = atoi(tmp);
            startValue = false;
            memset(tmp, 0, 10);
            count = 0;
            continue;
        }

        tmp[count] = c;
        count++;
    }
    
    free(tmp);
};

Request::~Request()
{
    for (int i = 0; i < this->countParams; i++)
    {
        delete[] this->params[i];
    }
    delete[] this->params;
    delete[] this->data;
};

int Request::getData(const char *str)
{
    for (int i = 0; i < this->countParams; i++)
    {
        if (!strcmp(this->params[i], str))
        {
            this->hasParamFlag = true;
            return this->data[i];
        }
    }

    this->hasParamFlag = false;
    return 0;
};

bool Request::hasParam()
{
    return this->hasParamFlag;
}
