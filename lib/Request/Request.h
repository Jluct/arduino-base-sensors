#pragma once

#include <Arduino.h>

class Request{
    private:
        char _GET[30];
        int countGetRequest = 0;

        int countParams = 0;
        char **params;
        int *data;

        bool isRequestRecord = false;
        bool hasParamFlag = false;

    public : 
        void setRequestGET(char symbol);
        void analyze();
        int getData(const char *str);
        bool hasParam();
        ~Request();
};