#pragma once

#include <Arduino.h>

struct Sensor
{
    unsigned int id;
    unsigned int pin;
    unsigned int location;
    unsigned int durationValidaty;
    unsigned int (*handler)(Sensor *sensor);
    unsigned long int lastCall;
    unsigned int data;
};