#include <Arduino.h>
#include "Sensor.h"
#include "SensorsContainer.h"
#include "Converter.h"

void SensorsContainer::callSensor()
{
    if (!this->state)
    {
        return;
    }

    unsigned long int curentMillis = millis();

    if ((curentMillis - this->sensors[this->currentSensor]->lastCall) >= this->sensors[this->currentSensor]->durationValidaty)
    {
        this->sensors[this->currentSensor]->data = this->sensors[this->currentSensor]->handler(this->sensors[this->currentSensor]);
        this->sensors[this->currentSensor]->lastCall = curentMillis;
    }

    if (this->currentSensor == this->countSensors - 1)
    {
        this->currentSensor = 0;
    }
    else
    {
        this->currentSensor++;
    }
};

void SensorsContainer::setState(bool state)
{
    this->state = (bool)state;
};

void SensorsContainer::setSensor(Sensor *sensor)
{
    this->sensors = (Sensor **)realloc(this->sensors, (this->countSensors + 1) * sizeof(Sensor *));
    this->sensors[this->countSensors] = sensor;
    this->countSensors++;
};

// не работает
bool SensorsContainer::getSensor(int number, Sensor *sensor)
{
    if (number < this->countSensors)
    {
        sensor = this->sensors[number];
        return true;
    }

    return false;
};

int SensorsContainer::getSensorCount()
{
    return this->countSensors;
};

String SensorsContainer::report(int TYPE, int location = 0)
{
    String report;
    switch (TYPE)
    {
    case 1:
        report = Converter::toJSON(this->sensors, this->countSensors, location);
        break;
    };

    return report;
};

bool SensorsContainer::callDevice(unsigned int device, int data)
{
    for (int i = 0; i < this->countDevices; i++)
    {
        if (this->devices[i]->id == device)
        {
            return (bool)this->devices[i]->handler(data);
        }
    }

    return false;
};

void SensorsContainer::addDevice(Device *device)
{
    this->devices = (Device **)realloc(this->devices, (this->countDevices + 1) * sizeof(Device *));
    this->devices[this->countDevices] = device;
    this->countDevices++;
};

SensorsContainer::~SensorsContainer()
{
    for (int i = 0; i < this->countDevices; i++)
    {

        free(this->devices[i]);
    }
    for (int i = 0; i < this->countSensors; i++)
    {

        free(this->sensors[i]);
    }
    free(this->sensors);
    free(this->devices);
}