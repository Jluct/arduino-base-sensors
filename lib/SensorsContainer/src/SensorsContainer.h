#pragma once

#include <Arduino.h>
#include "Sensor.h"
#include "Device.h"


// TODO: Переименовать, т.к. будут храниться и сенсоры и датчики
class SensorsContainer
{
private:
  int currentSensor = 0;
  bool state = false;

  int countSensors = 0;
  Sensor **sensors;

  int countDevices = 0;
  Device **devices;

public:
  ~SensorsContainer();
  const int TYPE_JSON = 1;
  const int TYPE_CSV = 2;

  void callSensor();
  String report(int TYPE, int location = 0);
  int getSensorCount();
  void setState(bool state);
  void setSensor(Sensor *sensor);
  bool getSensor(int number, Sensor *sensor);

  bool callDevice(unsigned int device, int data);
  void addDevice(Device *device);
};