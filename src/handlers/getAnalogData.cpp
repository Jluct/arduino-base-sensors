#pragma once
#include "Sensor.h"
#include "handlers/getAnalogData.h"

unsigned int getAnalogData(Sensor *sensor)
{
    return analogRead(sensor->pin);
};
