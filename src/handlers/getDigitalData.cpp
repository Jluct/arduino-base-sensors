#pragma once
#include "Sensor.h"
#include "handlers/getDigitalData.h"

unsigned int getDigitalData(Sensor *sensor)
{
    return digitalRead(sensor->pin);
}