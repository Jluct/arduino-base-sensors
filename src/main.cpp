/**
 * Получить показания с датчиков:
 * http://192.168.101.2/?location=1
 * где 1 - номер локации с которой вы хотите получить показания датчиков
 * 
 * Управление устройством:
 * http://192.168.101.2/?device=1&data=0
 * device - id устройства
 * data - данные, которые будут переданы в функцию-обработчик
 */

#include <Arduino.h>
#include <avr/wdt.h>
#include <SPI.h>
#include <Ethernet.h>
#include "Sensor.h"
#include "SensorsContainer.h"
#include "Request.h"
#include "Device.h"

#include "handlers/getAnalogData.h"
#include "handlers/getDigitalData.h"

char serialMessages[2][14] = {
    "new client\0", "disconnected\0"};

SensorsContainer container;

int pwmControll(int data)
{
    int pin = 6;
    analogWrite(pin, data);

    return 1;
};

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

IPAddress ip(192, 168, 101, 2);

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);

struct Sensor sensorLight = {1, A0, 1, 1000, getAnalogData, 1, getAnalogData(&sensorLight)};
struct Sensor sensorLed = {2, 7, 1, 1000, getDigitalData, 1, getDigitalData(&sensorLed)};

struct Device led = {8, 1, pwmControll};

void setup()
{
    wdt_enable(WDTO_4S);
    wdt_reset();

    //TODO: Вынести в функцию инициализации устройства
    pinMode(6, OUTPUT);
    pinMode(7, INPUT);

    // Open serial communications and wait for port to open:
    Serial.begin(9600);
    randomSeed(analogRead(A6));
    while (!Serial)
    {
        ; // wait for serial port to connect. Needed for native USB port only
    }

    container.setSensor(&sensorLight);
    container.setSensor(&sensorLed);

    container.addDevice(&led);

    container.setState(true);

    // start the Ethernet connection and the server:
    Ethernet.begin(mac, ip);
    server.begin();
    Serial.println(Ethernet.localIP());
    Serial.println("");
}

void loop()
{
    wdt_reset();

    // listen for incoming clients
    EthernetClient client = server.available();
    if (client)
    {
        // REQUEST //
        Request request = Request();
        Serial.println(serialMessages[0]);
        // an http request ends with a blank line
        boolean currentLineIsBlank = true;
        while (client.connected())
        {
            if (client.available())
            {
                char c = client.read();
                /**
                 * For debug 
                 */
                // Serial.write(c);
                request.setRequestGET(c);

                if (c == '\n' && currentLineIsBlank)
                {
                    request.analyze();

                    // send a standard http response header
                    client.println("HTTP/1.1 200 OK");
                    client.println("Content-Type: application/json");
                    client.println("Access-Control-Allow-Origin: *");
                    // client.println("Server: arduino-http");
                    // client.println("Connection: Keep-Alive");
                    // client.println("Refresh: 2"); // refresh the page automatically
                    client.println();

                    bool hasParam = false;

                    int device = request.getData("device");
                    if (request.hasParam())
                    {
                        char status[18] = "{\"status\": ";
                        {
                            char tmp[8];
                            strcat(status, itoa((int)container.callDevice(device, request.getData("data")), tmp, 10));
                        }
                        strcat(status, "}");
                        client.println(status);
                        break;
                    }

                    int location = request.getData("location");
                    if (request.hasParam())
                    {
                        client.println(
                            container.report(
                                container.TYPE_JSON,
                                location));

                        break;
                    }
                }
                if (c == '\n')
                {
                    // you're starting a new line
                    currentLineIsBlank = true;
                }
                else if (c != '\r')
                {
                    // you've gotten a character on the current line
                    currentLineIsBlank = false;
                }
            }
        }

        // give the web browser time to receive the data
        delay(100);
        // close the connection:
        client.stop();
        Serial.println(serialMessages[1]);
    }
    else
    {
        // сбор показаний с датчиков
        container.callSensor();
    }

    wdt_reset();
}